[![Software License](https://img.shields.io/badge/license-GPL-brightgreen.svg?style=flat)](LICENSE)

vtCMS
======

**_VoyaTraX Content Management System_ (vtCMS)** is an open-source Object-Oriented 
Web publishing system written in PHP. 

It is an ideal tool for developing small to large dynamic community websites, 
intra company portals, corporate portals, weblogs and much more.

Basing vtCMS on the well known Web Application project called XOOPS, was a very long and carefully planned objective.
The base is very reliable, easy and stable. It allows multilingual integretion, easily adding themes and modules.



vtCMS2.5.x Requirements
------------

- PHP 5.6+  (7.0+ HIGHLY RECOMMENDED)
- MySQL 5.1+
- Apache2 or nginX server.


Installation
------------

We will create a wiki as soon as possable.


Licenses
-------

* We keep the same license that is used by the parent project(s) - usually GPLv2.
* Themes we create will be under a CC license.
* Modules we create will generally use the BSD or GPL License.
* It may also happen that we use the MIT License, but this would be rare.


** _Be sure to read the license details within each project._ **


Modules Included
-------

_**NOTE: not all of these listed will be kept in core! Only the DENDENCIES will be kept.**_


 * Contact
 
 ~~* extCal~~
 
 ~~* extGallery~~
 
 ~~* vtForum~~
 
 * News
 
 ~~* wfDownloads~~
 
 * Tags
 * xLanguage
 * xNewsletter
 * xoopsHeadline
 * xoopsMembers
 
 ~~* xoopsPartners~~
 
 ~~* xoopsPoll~~
 
 * xSitemap
 
 ~~* xoopsfaq~~
 
 ~~* fmContent~~
 
 ~~* gWiki~~

**ATTENTION:** _the above is alist I suggest using. See issue#21 for details._


Themes Included
-------

 * default - using 960 as a skeleton to build off of.  - See issue#22
 * 960_vtThemeSkeleton - the 960 theme with vtCMS stuff added.
 * Belius - ported from ImpressCMS
 * xBootStrap - already in the base system


The key features
---------

 * Database-driven
 * Fully Modularized
 * Personalization
 * User Management
 * Supported World-wide
 * Multi-byte Language Support
 * Versatile Group Permissions System
 * Theme-based skinnable interface
 * About Us page - admin managable
 * Disclaimer page - admin managable
 * Terms page - admin managable
 * Imprint page - admin managable
 * Privacy Policy page - admin managable
 * Javascript to add _blinking text_ to your site.
 * European Cookie Law Compliant with euCookie - easy to integret into your theme.
 * German & English included
 
 
Releases
----------
 
 Releases will be done as the development matures to a (semi-)stable state. What
 this means is if everything installs and work well enough, we release.
 
 Releases are done in the _Semantic Versioning Method_, as can be followed @ http://semver.org/
 
   * **Semantic Versioning Summary

  Given a version number MAJOR.MINOR.PATCH, increment the:

  - MAJOR version when you make incompatible API changes,
  - MINOR version when you add functionality in a backwards-compatible manner, and
  - PATCH version when you make backwards-compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.
