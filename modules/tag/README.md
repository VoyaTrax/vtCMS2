![alt XOOPS CMS](http://xoops.org/images/logoXoops4GithubRepository.png)
## Tag module for  [XOOPS CMS 2.5.8+](https://xoops.org)
[![Software License](https://img.shields.io/badge/license-GPL-brightgreen.svg?style=flat)](LICENSE)
[![Scrutinizer Code Quality](https://img.shields.io/scrutinizer/g/XoopsModules25x/tag.svg?style=flat)](https://scrutinizer-ci.com/g/XoopsModules25x/tag/?branch=master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/0fdcfa1725a14753865921b99bb3611e)](https://www.codacy.com/app/mambax7/tag_2)
[![Code Climate](https://img.shields.io/codeclimate/github/XoopsModules25x/tag.svg?style=flat)](https://codeclimate.com/github/XoopsModules25x/tag)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/0ffc2915-82a8-446a-978e-df50e2e58858/mini.png)](https://insight.sensiolabs.com/projects/0ffc2915-82a8-446a-978e-df50e2e58858)
[![Latest Pre-Resease](https://img.shields.io/github/tag/XoopsModules25x/tag.svg?style=flat)](https://github.com/XoopsModules25x/tag/tags/)
[![Latest Version](https://img.shields.io/github/release/XoopsModules25x/tag.svg?style=flat)](https://github.com/XoopsModules25x/tag/releases/)

Tag module for [XOOPS CMS](http://xoops.org) is designed for site-wide tag management, handling tag input, display and stats for each module that enables tag plugin.

[![Tutorial Available](http://xoops.org/images/tutorial-available-blue.svg)](https://www.gitbook.com/book/xoops/tag-tutorial/) Tutorial: see [GitBook](https://www.gitbook.com/book/xoops/tag-tutorial/).
To contribute to the Tutorial, [fork it on GitHub](https://github.com/XoopsDocs/tag-tutorial)

[![Translations on Transifex](http://xoops.org/images/translations-transifex-blue.svg)](https://www.transifex.com/xoops)

Please visit us on http://xoops.org

The upcoming "next generation" version of XOOPS CMS is being crafted on GitHub at: https://github.com/XOOPS

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/0fdcfa1725a14753865921b99bb3611e)](https://www.codacy.com/app/mambax7/tag_2?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=XoopsModules25x/tag&amp;utm_campaign=Badge_Grade)
