<?php
/**
 *  Xoops Spanish Support (http://www.esxoops.com)
 *
 */

define('_MD_XPARTNERS_JOIN', 'Unirte a los Sitios Asociados');
define('_MD_XPARTNERS_PARTNERS', 'Sitios Asociados');
define('_MD_XPARTNERS_PARTNER', 'Asociado');
define('_MD_PARTNERS_DESCRIPTION', 'Descripción');
define('_MD_PARTNERS_HITS', 'Hits');
define('_MD_XPARTNERS_NOPART', 'No hay Sitios Asociados.');
//file join.php
define('_MD_XPARTNERS_IMAGE', 'Imagen:');
define('_MD_XPARTNERS_TITLE', 'Título:');
define('_MD_XPARTNERS_URL', 'URL:');
define('_MD_XPARTNERS_SEND', 'Enviar Email');
define('_MD_XPARTNERS_ERROR1', "<span style='text-align: center;'>" . "Se han encontrado algún error, <a href='javascript:history.back(1)'>intentelo de nuevo</a>" . '</span>');
define('_MD_XPARTNERS_ERROR2', "<span style='text-align: center;'>" . "La imagen es más grande de 110x50, <a href='javascript:history.back(1)'>intentelo con otra imagen</a>" . '</span>');
define('_MD_XPARTNERS_ERROR3', "<span style='text-align: center;'>" . "La imagen no existe, <a href='javascript:history.back(1)'>intentelo con otra imagen</a>" . '</span>');
define('_MD_XPARTNERS_ERROR4', "<style='text-align: center;'>" . "The image file type is invalid. <a href='javascript:history.back(1)'>Try with another image</a></span>");
define('_MD_XPARTNERS_GOBACK', "<a href='javascript:history.back(1)'>intentelo de nuevo</a>");
define('_MD_XPARTNERS_NEWPARTNER', 'Nueva solicitud para unirse a los Sitios Asociados de %s');
define('_MD_XPARTNERS_SENDMAIL', "Se envio un email con su solicitud al administrador<br /><a href='index.php'>Volver al Indice</a>");
// 1.11
//file /admin/index.php
define('_MD_XPARTNERS_DASHBOARD', 'XOOPS Partner Dashboard');
define('_MD_XPARTNERS_TOTALPARTNERS', 'Total Partners: <strong>%s</strong> ');
define('_MD_XPARTNERS_TOTALACTIVE', 'Active Partners: <strong>%s</strong> ');
define('_MD_XPARTNERS_TOTALNONACTIVE', 'Non-Active Partners: <strong>%s</strong> ');
//define("_MD_XPARTNERS_TOTALPARTNERS","There are <strong>%s</strong> Active Partners in our Database");
