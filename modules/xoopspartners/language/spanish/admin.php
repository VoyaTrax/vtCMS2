<?php // Xoops Spanish Support (http://www.esxoops.com)
// $Id: admin.php 8212M 2011-11-07 04:37:28Z (local) $
$adminMyDirName = basename(dirname(dirname(__DIR__)));

define('_AM_XPARTNERS_URL', 'URL');
define('_AM_XPARTNERS_HITS', 'Hits');
define('_AM_XPARTNERS_IMAGE', 'Imágen');
define('_AM_XPARTNERS_TITLE', 'Título');
define('_AM_XPARTNERS_WEIGHT', 'Orden');
define('_AM_XPARTNERS_DESCRIPTION', 'Descripción');
define('_AM_XPARTNERS_STATUS', 'Estado');
define('_AM_XPARTNERS_ACTIVE', 'Activo');
define('_AM_XPARTNERS_INACTIVE', 'Inactivo');
define('_AM_XPARTNERS_REORDER', 'Actualizar');
define('_AM_XPARTNERS_UPDATED', 'Configuración Actualizada!');
define('_AM_XPARTNERS_NOTUPDATED', 'No puedo actualizar la configuración!');
define('_AM_XPARTNERS_BESURE', 'Asegurate de introducir como mínimo el título, el URL, y la descripción.');
define('_AM_XPARTNERS_NOEXIST', 'Archivo de imágen no existe');
define('_AM_XPARTNERS_ADDPARTNER', 'Añadir Sitio Asociado');
define('_AM_XPARTNERS_EDITPARTNER', 'Editar');
define('_AM_XPARTNERS_SUREDELETE', '¿Está seguro de querer borrar este Sitio?');
define('_AM_XPARTNERS_IMAGE_ERROR', 'Tamaño de la imagen es superior de 150x80');
define('_AM_XPARTNERS_ADD', 'Agregar Sitios');
define('_AM_XPARTNERS_AUTOMATIC_SORT', 'Reordenar automáticamente');
define('_AM_XPARTNERS_UPDATE', 'Actualizar');

//1.11

// About.php
define('_AM_XPARTNERS_ABOUT_RELEASEDATE', 'Released: ');
define('_AM_XPARTNERS_ABOUT_UPDATEDATE', 'Updated: ');
define('_AM_XPARTNERS_ABOUT_AUTHOR', 'Author: ');
define('_AM_XPARTNERS_ABOUT_CREDITS', 'Credits: ');
define('_AM_XPARTNERS_ABOUT_LICENSE', 'License: ');
define('_AM_XPARTNERS_ABOUT_MODULE_STATUS', 'Status: ');
define('_AM_XPARTNERS_ABOUT_WEBSITE', 'Website: ');
define('_AM_XPARTNERS_ABOUT_AUTHOR_NAME', 'Author name: ');
define('_AM_XPARTNERS_ABOUT_CHANGELOG', 'Change Log');
define('_AM_XPARTNERS_ABOUT_MODULE_INFO', 'Module Infos');
define('_AM_XPARTNERS_ABOUT_AUTHOR_INFO', 'Author Infos');
define('_AM_XPARTNERS_ABOUT_DESCRIPTION', 'Description: ');
define('_AM_XPARTNERS_EMPTYDATABASE', 'There is nothing to sort. Please add some Partners first!');

// Configuration
define('_AM_XPARTNERS_CONFIG_CHECK', 'Configuration Check');
define('_AM_XPARTNERS_CONFIG_PHP', 'Minimum PHP required: %s (your version is %s)');
define('_AM_XPARTNERS_CONFIG_XOOPS', 'Minimum XOOPS required: %s (your version is %s)');

define('_AM_XPARTNERS_ACTIONS', 'Actions');
define('_AM_XPARTNERS_INVALIDID', 'No partner exists with this ID');

// text in admin footer
define('_AM_XPARTNERS_ADMIN_FOOTER', "<div class='center smallsmall italic pad5'><strong>{$adminMyDirName}</strong> is maintained by the <a class='tooltip' rel='external' href='http://xoops.org/' title='Visit XOOPS Community'>XOOPS Community</a></div>");
