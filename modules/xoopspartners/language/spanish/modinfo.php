<?php
/**
 * Xoops Spanish Support (http://www.esxoops.com)
 **/

define('_MI_XPARTNERS_NAME', 'Sitios Asociados');
define('_MI_XPARTNERS_DESC', 'Mostrar Sitios asociados en un Bloque y en un modulo');
define('_MI_XPARTNERS_ADMIN', 'Manejar Sitios');
define('_MI_XPARTNERS_ADMIN_ADD', 'Agregar Sitios');
define('_MI_XPARTNERS_HITS', 'Hits');
define('_MI_XPARTNERS_TITLE', 'Título');
define('_MI_XPARTNERS_WEIGHT', 'Orden');
define('_MI_XPARTNERS_RECLICK', 'Tiempo de Reclick:');
define('_MI_XPARTNERS_IMAGES', 'Imagenes');
define('_MI_XPARTNERS_TEXT', 'Texto de enlace');
define('_MI_XPARTNERS_BOTH', 'Ambos');
define('_MI_XPARTNERS_MLIMIT', 'Limitar a xx entradas en la página principal: (0 = sin límite)');
define('_MI_XPARTNERS_MLIMITDSC', '');
define('_MI_XPARTNERS_MSHOW', 'En página principal mostrar por:');
define('_MI_XPARTNERS_MSHOWDSC', '');
define('_MI_XPARTNERS_MORDER', 'Orden de contenido de la p�gina principal:');
define('_MI_XPARTNERS_MORDERDSC', '');
define('_MI_XPARTNERS_MSORT', 'Tipo de contenido de la p�gina principal:');
define('_MI_XPARTNERS_MSORTDSC', '');
define('_MI_XPARTNERS_ID', 'ID');
define('_MI_XPARTNERS_HOUR', '1 hora');
define('_MI_XPARTNERS_3HOURS', '3 horas');
define('_MI_XPARTNERS_5HOURS', '5 horas');
define('_MI_XPARTNERS_10HOURS', '10 horas');
define('_MI_XPARTNERS_DAY', '1 dia');

//1.11
// index.php
define('_MI_XPARTNERS_ADMIN_HOME', 'Inicio');
define('_MI_XPARTNERS_ADMIN_ABOUT', 'About');
define('_MI_XPARTNERS_ADMIN_HELP', 'Help');
define('_MI_XPARTNERS_ADMIN_MANAGE', 'Admin');
define('_MI_XPARTNERS_ADMIN_ADDP', 'Agregar Sitios');
define('_MI_XPARTNERS_INCADMIN', 'Incluir Admin hits en contra');

// template descriptions
define('_MI_XPARTNERS_TMPLT1_DESC', 'Partners main screen');
define('_MI_XPARTNERS_TMPLT2_DESC', 'Shows Join to the partners form');
