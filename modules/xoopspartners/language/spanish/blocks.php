<?php // Xoops Spanish Support (http://www.esxoops.com)
// $Id: blocks.php 8212 2011-11-07 04:37:28Z beckmi $
define('_MB_XPARTNERS_PARTNERS_PSPACE', '�Poner saltos entre los socios??');
define('_MB_XPARTNERS_BRAND', '�Poner en orden aleatorio en bloques?');
define('_MB_XPARTNERS_BLIMIT', 'Limitar a xx entradas en Bloques: (0 = Sin l�mite)');
define('_MB_XPARTNERS_BSHOW', 'En bloques mostrar por:');
define('_MB_XPARTNERS_BSORT', 'En bloques ordenar por:');
define('_MB_XPARTNERS_IMAGES', 'Imagenes');
define('_MB_XPARTNERS_TEXT', 'Texto de enlace');
define('_MB_XPARTNERS_BOTH', 'Ambos');
define('_MB_XPARTNERS_HITS', 'Hits');
define('_MB_XPARTNERS_TITLE', 'Título');
define('_MB_XPARTNERS_WEIGHT', 'Orden');
define('_MB_XPARTNERS_ASC', 'Ascendente');
define('_MB_XPARTNERS_DESC', 'Descendente');
define('_MB_XPARTNERS_TTL_LENGTH', 'Nombre de caract�res � afficher dans le titre (0 = Sin l�mite)');
define('_MB_XPARTNERS_ID', 'ID');
define('_MB_XPARTNERS_FADE', 'Decolorar imagen');
