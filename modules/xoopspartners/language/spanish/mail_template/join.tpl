Hola.

Recibiste una solicitud de entrada al partner de tu site {SITENAME},
La solicitud fue enviada por el usuario: {USER} de ip: {IP}
La información que mando para la solicitud fue:

Título: {TITLE}
URL:    {URL}
Imagen: {IMAGE}
Descripción:
{DESCRIPTION}

Puedes añadir esta solicitud desde esta dirección:
{SITEURL}modules/{MODULENAME}/admin/main.php?op=partners_manage_add

-----------
{SITENAME} ({SITEURL})
