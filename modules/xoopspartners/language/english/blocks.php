<?php
/*

  ------------------------------------------------------------------------
                XOOPS - PHP Content Management System
                    Copyright (c) 2000 XOOPS.org
                       <http://www.xoops.org/>
  ------------------------------------------------------------------------
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  You may not change or alter any portion of this comment or credits
  of supporting developers from this source code or any supporting
  source code which is considered copyrighted (c) material of the
  original comment or credit authors.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
  ------------------------------------------------------------------------
 Author: Raul Recio (AKA UNFOR)
 Project: The XOOPS Project
 -------------------------------------------------------------------------
 */
/**
 * XoopsPartners - a partner affiliation links module
 *
 * @category     Module
 * @package      xoopspartners
 * @subpackage   language
 * @author       Raul Recio (aka UNFOR)
 * @author       XOOPS Module Development Team
 * @copyright    {@link http://xoops.org 2001-2016 XOOPS Project}
 * @license      {@link http://www.gnu.org/licenses/gpl-2.0.html GNU Public License}
 * @link         http://xoops.org XOOPS
 */

define('_MB_XPARTNERS_PSPACE', 'Put breaks between partners?');
define('_MB_XPARTNERS_BRAND', 'Randomize partners in blocks?');
define('_MB_XPARTNERS_BLIMIT', 'Limit blocks to xx entries: (0 = no limit)');
define('_MB_XPARTNERS_BSHOW', 'In blocks show:');
define('_MB_XPARTNERS_BSORT', 'Sort block content by:');
define('_MB_XPARTNERS_ID', 'ID');
define('_MB_XPARTNERS_HITS', 'Hits');
define('_MB_XPARTNERS_TITLE', 'Title');
define('_MB_XPARTNERS_WEIGHT', 'Weight');
define('_MB_XPARTNERS_ASC', 'Ascending');
define('_MB_XPARTNERS_DESC', 'Descending');
define('_MB_XPARTNERS_TTL_LENGTH', 'Num chars to show in title (0 = no limit)');
define('_MB_XPARTNERS_IMAGES', 'Images');
define('_MB_XPARTNERS_TEXT', 'Text Links');
define('_MB_XPARTNERS_BOTH', 'Both');
define('_MB_XPARTNERS_FADE', 'Fade Image');
