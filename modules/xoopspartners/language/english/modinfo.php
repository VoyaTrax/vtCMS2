<?php
/*
  ------------------------------------------------------------------------
                XOOPS - PHP Content Management System
                    Copyright (c) 2000 XOOPS.org
                       <http://www.xoops.org/>
  ------------------------------------------------------------------------
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  You may not change or alter any portion of this comment or credits
  of supporting developers from this source code or any supporting
  source code which is considered copyrighted (c) material of the
  original comment or credit authors.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
  ------------------------------------------------------------------------
 Author: Raul Recio (AKA UNFOR)
 Project: The XOOPS Project
 -------------------------------------------------------------------------
 */
/**
 * XoopsPartners - a partner affiliation links module
 *
 * @category     Module
 * @package      xoopspartners
 * @subpackage   language
 * @author       Raul Recio (aka UNFOR)
 * @author       XOOPS Module Development Team
 * @copyright    {@link http://xoops.org 2001-2016 XOOPS Project}
 * @license      {@link http://www.gnu.org/licenses/gpl-2.0.html GNU Public License}
 * @link         http://xoops.org XOOPS
 */

define('_MI_XPARTNERS_NAME', 'Partners');
define('_MI_XPARTNERS_DESC', 'Shows partner sites in a block and in a module');
define('_MI_XPARTNERS_ADMIN', 'Manage Partners');
define('_MI_XPARTNERS_ADMIN_ADD', 'Add Partner');
define('_MI_XPARTNERS_ID', 'ID');
define('_MI_XPARTNERS_HITS', 'Hits');
define('_MI_XPARTNERS_TITLE', 'Title');
define('_MI_XPARTNERS_WEIGHT', 'Weight');
define('_MI_XPARTNERS_RECLICK', 'Reclick Time:');
define('_MI_XPARTNERS_IMAGES', 'Images');
define('_MI_XPARTNERS_TEXT', 'Text Links');
define('_MI_XPARTNERS_BOTH', 'Both');
define('_MI_XPARTNERS_MLIMIT', 'Limit main page to xx entries: (0 = no limit)');
define('_MI_XPARTNERS_MLIMITDSC', '');
define('_MI_XPARTNERS_MSHOW', 'On main page show:');
define('_MI_XPARTNERS_MSCHODSC', '');
define('_MI_XPARTNERS_MORDER', 'Order main page content by:');
define('_MI_XPARTNERS_MORDERDSC', '');
define('_MI_XPARTNERS_MSORT', 'Sort main page content by:');
define('_MI_XPARTNERS_MSORTDSC', '');
define('_MI_XPARTNERS_HOUR', '1 hour');
define('_MI_XPARTNERS_3HOURS', '3 hours');
define('_MI_XPARTNERS_5HOURS', '5 hours');
define('_MI_XPARTNERS_10HOURS', '10 hours');
define('_MI_XPARTNERS_DAY', '1 day');

//1.11-1.12
// index.php
define('_MI_XPARTNERS_ADMIN_HOME', 'Home');
define('_MI_XPARTNERS_ADMIN_ABOUT', 'About');
define('_MI_XPARTNERS_ADMIN_HELP', 'Help');
define('_MI_XPARTNERS_ADMIN_MANAGE', 'Management');
define('_MI_XPARTNERS_ADMIN_ADDP', 'Add Partner');
define('_MI_XPARTNERS_UPLOADFILESIZE', 'Maximum Filesize Upload (KB) 1048576 = 1 Meg');
define('_MI_XPARTNERS_IMAGE_MAX_WIDTH', 'Maximum image width');
define('_MI_XPARTNERS_IMAGE_MAX_HEIGHT', 'Maximum image height');
define('_MI_XPARTNERS_INCADMIN', 'Include Admin hits in counter');

// template descriptions
define('_MI_XPARTNERS_TMPLT1_DESC', 'Partners main screen');
define('_MI_XPARTNERS_TMPLT2_DESC', 'Shows Join to the partners form');
define('_MI_XPARTNERS_UPLOADFILESIZE_DESC', 'This is the max. size file before resizing.');
define('_MI_XPARTNERS_IMAGE_MAX_WIDTH_DESC', "Width <span style='text-decoration: underline;'>after</span> it is resized");
define('_MI_XPARTNERS_IMAGE_MAX_HEIGHT_DESC', "Height <span style='text-decoration: underline;'>after</span> it is resized");
