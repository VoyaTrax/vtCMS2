<?php
/*
  ------------------------------------------------------------------------
                XOOPS - PHP Content Management System
                    Copyright (c) 2000 XOOPS.org
                       <http://www.xoops.org/>
  ------------------------------------------------------------------------
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  You may not change or alter any portion of this comment or credits
  of supporting developers from this source code or any supporting
  source code which is considered copyrighted (c) material of the
  original comment or credit authors.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
  ------------------------------------------------------------------------
 Author: Raul Recio (AKA UNFOR)
 Project: The XOOPS Project
 -------------------------------------------------------------------------
 */
/**
 * XoopsPartners - a partner affiliation links module
 * @category     Module
 * @package      xoopspartners
 * @subpackage   language
 * @author       Raul Recio (aka UNFOR)
 * @author       XOOPS Module Development Team
 * @copyright    {@link http://xoops.org 2001-2016 XOOPS Project}
 * @license      {@link http://www.gnu.org/licenses/gpl-2.0.html GNU Public License}
 * @link         http://xoops.org XOOPS
 */

define('_MD_XPARTNERS_JOIN', 'Become a Partner');
define('_MD_XPARTNERS_PARTNERS', 'Partners');
define('_MD_XPARTNERS_PARTNER', 'Partner');
define('_MD_XPARTNERS_DESCRIPTION', 'Description');
define('_MD_XPARTNERS_HITS', 'Hits');
define('_MD_XPARTNERS_NOPART', 'No such partner.');
//file join.php
define('_MD_XPARTNERS_IMAGE', 'Image:');
define('_MD_XPARTNERS_TITLE', 'Title:');
define('_MD_XPARTNERS_URL', 'URL:');
define('_MD_XPARTNERS_SEND', 'Send Email');
define('_MD_XPARTNERS_ERROR1', "<center>ERROR: There was an error. <a href='javascript:history.back(1)'>Try again</a></center>");
define('_MD_XPARTNERS_ERROR2', "<span style='text-align: center'>" . "Image size is larger than 150x110. <a href='javascript:history.back(1)'>Try with another image</a></span>");
define('_MD_XPARTNERS_ERROR3', "<style='text-align: center;'>" . "The %s image file doesn't exist. <a href='javascript:history.back(1)'>Try with another image</a></span>");
define('_MD_XPARTNERS_ERROR4', "<style='text-align: center;'>" . "The image file type is invalid. <a href='javascript:history.back(1)'>Try with another image</a></span>");
define('_MD_XPARTNERS_GOBACK', "<a href='javascript:history.back(1)'>Back</a>");
define('_MD_XPARTNERS_NEWPARTNER', '%s Partners Request');
define('_MD_XPARTNERS_SENDMAIL', "Request mail sent to the administrator.<br><a href='index.php'>Return to index</a>");

// 1.11
//file /admin/index.php
define('_MD_XPARTNERS_DASHBOARD', 'XOOPS Partner Dashboard');
define('_MD_XPARTNERS_TOTALPARTNERS', 'Total Partners: <strong>%s</strong> ');
define('_MD_XPARTNERS_TOTALACTIVE', 'Active Partners: <strong>%s</strong> ');
define('_MD_XPARTNERS_TOTALNONACTIVE', 'Non-Active Partners: <strong>%s</strong> ');
//define('_MD_XPARTNERS_TOTALPARTNERS', "There are <strong>%s</strong> Active Partners in our Database");
