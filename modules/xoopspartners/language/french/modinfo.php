<?php
/**
 * Support Francophone de Xoops (www.frxoops.org)
 * @translation::     Communauté XOOPS Francophone
 * @specification::   _LANGCODE: fr
 * @specification::   _CHARSET: UTF-8 sans bom
 *
 **/
//
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //

define('_MI_XPARTNERS_NAME', 'Partenaires');
define('_MI_XPARTNERS_DESC', 'Affiche les partenaires du site dans un bloc et dans un module');
define('_MI_XPARTNERS_ADMIN', 'Gestion des partenaires');
define('_MI_XPARTNERS_ADMIN_ADD', 'Ajouter un partenaire');
define('_MI_XPARTNERS_ID', 'ID');
define('_MI_XPARTNERS_HITS', 'Hits');
define('_MI_XPARTNERS_TITLE', 'Titre');
define('_MI_XPARTNERS_WEIGHT', 'Poids');
define('_MI_XPARTNERS_RECLICK', 'Délai reclic :');
define('_MI_XPARTNERS_IMAGES', 'Images');
define('_MI_XPARTNERS_TEXT', 'Texte des liens');
define('_MI_XPARTNERS_BOTH', 'Les deux');
define('_MI_XPARTNERS_MLIMIT', 'Limiter la page principale à xx entrées: (0 = aucune limite)');
define('_MI_XPARTNERS_MLIMITDSC', '');
define('_MI_XPARTNERS_MSHOW', 'Afficher sur la page principale :');
define('_MI_XPARTNERS_MSHOWDSC', '');
define('_MI_XPARTNERS_MORDER', 'Ordre principal contenu de la page:');
define('_MIXPARTNERS_MORDERDSC', '');
define('_MI_XPARTNERS_MSORT', 'Tri principal contenu de la page:');
define('_MIXPARTNERS_MSORTDSC', '');
define('_MI_XPARTNERS_HOUR', '1 heure');
define('_MI_XPARTNERS_3HOURS', '3 heures');
define('_MI_XPARTNERS_5HOURS', '5 heures');
define('_MI_XPARTNERS_10HOURS', '10 heures');
define('_MI_XPARTNERS_DAY', '1 jour');
//1.11
// index.php
define('_MI_XPARTNERS_ADMIN_HOME', 'Accueil');
define('_MI_XPARTNERS_ADMIN_ABOUT', 'Infos');
define('_MI_XPARTNERS_ADMIN_HELP', 'Aide');
define('_MI_XPARTNERS_ADMIN_MANAGE', 'Admin');
define('_MI_XPARTNERS_ADMIN_ADDP', 'Ajouter un partenaire');
define('_MI_XPARTNERS_INCADMIN', 'Inclure Admin hits de compteur');

// template descriptions
define('_MI_XPARTNERS_TMPLT1_DESC', 'Partners main screen');
define('_MI_XPARTNERS_TMPLT2_DESC', 'Shows Join to the partners form');
