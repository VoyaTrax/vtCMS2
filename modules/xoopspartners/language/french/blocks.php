<?php
/* Support Francophone de Xoops (www.frxoops.org)
  ------------------------------------------------------------------------ //
                XOOPS - PHP Content Management System                      //
                    Copyright (c) 2000 XOOPS.org                           //
                       <http://www.xoops.org/>                             //
  ------------------------------------------------------------------------ //
  This program is free software; you can redistribute it and/or modify     //
  it under the terms of the GNU General Public License as published by     //
  the Free Software Foundation; either version 2 of the License, or        //
  (at your option) any later version.                                      //
                                                                           //
  You may not change or alter any portion of this comment or credits       //
  of supporting developers from this source code or any supporting         //
  source code which is considered copyrighted (c) material of the          //
  original comment or credit authors.                                      //
                                                                           //
  This program is distributed in the hope that it will be useful,          //
  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
  GNU General Public License for more details.                             //
                                                                           //
  You should have received a copy of the GNU General Public License        //
  along with this program; if not, write to the Free Software              //
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
  ------------------------------------------------------------------------ //
*/
/**
 * @translation     Communauté XOOPS Francophone
 * @specification   _LANGCODE: fr
 * @specification   _CHARSET: UTF-8 sans bom
 *
 **/
define('_MB_XPARTNERS_PARTNERS_PSPACE', 'Mettre un espace entre les partenaires?');
define('_MB_XPARTNERS_BRAND', 'Randomize partenaires de blocs?');
define('_MB_XPARTNERS_BLIMIT', 'Limiter les blocs � xx entr�es: (0 = Aucune limite)');
define('_MB_XPARTNERS_BSHOW', 'Afficher dans les blocs :');
define('_MB_XPARTNERS_BSORT', 'Contenu du bloc class� par :');
define('_MB_XPARTNERS_ID', 'ID');
define('_MB_XPARTNERS_HITS', 'Hits');
define('_MB_XPARTNERS_TITLE', 'Titre');
define('_MB_XPARTNERS_WEIGHT', 'Poids');
define('_MB_XPARTNERS_ASC', 'Ascendant');
define('_MB_XPARTNERS_DESC', 'Descendant');
define('_MB_XPARTNERS_TTL_LENGTH', 'Num de caracteres a mostrar en el t�tulo (0 = Aucune limite)');
define('_MB_XPARTNERS_IMAGES', 'Images');
define('_MB_XPARTNERS_TEXT', 'Textes Liens');
define('_MB_XPARTNERS_BOTH', 'Les deux');
define('_MB_XPARTNERS_FADE', "Opacit� de l'image");
