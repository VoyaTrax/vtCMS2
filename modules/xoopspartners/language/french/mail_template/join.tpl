Bonjour.

Vous avez reçu une demande de partenariat pour votre site {SITENAME}
La demande a été envoyé par l'utilisateur {USER} dont l'IP est {IP}
Les informations qui nous ont été fournies dans le formulaire joint était :

Títre : {TITLE}
URL :    {URL}
Image : {IMAGE}
Description :
{DESCRIPTION}

Vous pouvez ajouter cette demande de partenariat à partir de cette url :
{SITEURL}modules/{MODULENAME}/admin/main.php?op=partners_manage_add

-----------
{SITENAME} ({SITEURL})
