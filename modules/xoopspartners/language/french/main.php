<?php
/**
 * Support Francophone de Xoops (www.frxoops.org)
 *
 */
//
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //

define('_MD_XPARTNERS_JOIN', 'Devenez un partenaire');
define('_MD_XPARTNERS_PARTNERS', 'Partenaires');
define('_MD_XPARTNERS_PARTNER', 'Partenaire');
define('_MD_XPARTNERS_DESCRIPTION', 'Description');
define('_MD_XPARTNERS_HITS', 'Hits');
define('_MD_XPARTNERS_NOPART', 'Aucun partenaire dans la base de données.');
//file join.php
define('_MD_XPARTNERS_IMAGE', 'Image :');
define('_MD_XPARTNERS_TITLE', 'Titre :');
define('_MD_XPARTNERS_URL', 'URL :');
define('_MD_XPARTNERS_SEND', 'Envoyer E-mail');
define('_MD_XPARTNERS_ERROR1', "<span style='text-align: center;'>" . "ERREUR : Il y a eu une erreur. <a href='javascript:history.back(1)'>Essayez à nouveau</a>" . '</span>');
define('_MD_XPARTNERS_ERROR2', "<span style='text-align: center;'>" . "La taille de l'image est supérieur à 115x110. " . "<a href='javascript:history.back(1)'>Essayez avec une autre image</a>" . '</span>');
define('_MD_XPARTNERS_ERROR3', "<span style='text-align: center;'>" . "Le fichier image n'existe pas. <a href='javascript:history.back(1)'>Essayez avec une autre image</a>" . '</span>');
define('_MD_XPARTNERS_ERROR4', "<style='text-align: center;'>" . "The image file type is invalid. <a href='javascript:history.back(1)'>Try with another image</a></span>");
define('_MD_XPARTNERS_GOBACK', "<a href='javascript:history.back(1)'>Retour</a>");
define('_MD_XPARTNERS_NEWPARTNER', '%s Demande(s) de Partenariat');
define('_MD_XPARTNERS_SENDMAIL', "Votre demande a été envoyé par mail à l'administrateur.<br />" . "<a href='index.php'>Retour à l'index</a>");

/**
 * @translation     Communauté XOOPS Francophone
 * @specification   _LANGCODE: fr
 * @specification   _CHARSET: UTF-8 sans bom
 *
 */
// 1.11
//file /admin/index.php
define('_MD_XPARTNERS_DASHBOARD', 'XOOPS Partner Dashboard');
define('_MD_XPARTNERS_TOTALPARTNERS', 'Total Partners: <strong>%s</strong> ');
define('_MD_XPARTNERS_TOTALACTIVE', 'Active Partners: <strong>%s</strong> ');
define('_MD_XPARTNERS_TOTALNONACTIVE', 'Non-Active Partners: <strong>%s</strong> ');
//define('_MD_XPARTNERS_TOTALPARTNERS',"There are <strong>%s</strong> Active Partners in our Database");
