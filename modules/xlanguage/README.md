![alt XOOPS CMS](http://xoops.org/images/logoXoops4GithubRepository.png)
##xLanguage module for  [XOOPS CMS 2.5.8+](https://xoops.org)
[![Software License](https://img.shields.io/badge/license-GPL-brightgreen.svg?style=flat)](LICENSE)
[![Scrutinizer Code Quality](https://img.shields.io/scrutinizer/g/XoopsModules25x/xlanguage.svg?style=flat)](https://scrutinizer-ci.com/g/XoopsModules25x/xlanguage/?branch=master)
[![Codacy Badge](https://api.codacy.com/project/badge/grade/177664bcc7874ab7bddc1192c59e480d)](https://www.codacy.com/app/mambax7/xlanguage_2)
[![Code Climate](https://img.shields.io/codeclimate/github/XoopsModules25x/xlanguage.svg?style=flat)](https://codeclimate.com/github/XoopsModules25x/xlanguage)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/f9b47b9f-9d03-4d6f-a639-badc6e3b1475/mini.png)](https://insight.sensiolabs.com/projects/f9b47b9f-9d03-4d6f-a639-badc6e3b1475)
[![Latest Pre-Release](https://img.shields.io/github/tag/XoopsModules25x/xlanguage.svg?style=flat)](https://github.com/XoopsModules25x/xlanguage/tags/)
[![Latest Version](https://img.shields.io/github/release/XoopsModules25x/xlanguage.svg?style=flat)](https://github.com/XoopsModules25x/xlanguage/releases/)

xLanguage module for [XOOPS CMS](http://xoops.org) handles displaying contents in different languages, like English, French and Chinese. If you want to switch between languages, this module is for you.

[![Tutorial Available](http://xoops.org/images/tutorial-available-blue.svg)](https://www.gitbook.com/book/xoops/xlanguage-tutorial/) Tutorial: see [here](https://www.gitbook.com/book/xoops/xlanguage-tutorial/).
To contribute to the Tutorial, [fork it on GitHub](https://github.com/XoopsDocs/xlanguage-tutorial)

[![Translations on Transifex](http://xoops.org/images/translations-transifex-blue.svg)](https://www.transifex.com/xoops)

Please visit us on  [http://xoops.org](http://xoops.org)

The upcoming "next generation" version of XOOPS CMS is being crafted on GitHub at: https://github.com/XOOPS

