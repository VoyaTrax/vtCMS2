<?php
// 
// _LANGCODE: en
// _CHARSET : UTF-8
// Translator: XOOPS Translation Team

$content .=
"<h3>Your site</h3>
<p>You can now access the <a href='../index.php'><font color='blue'>home page of your site</font></a>.</p>
<h3>Support</h3>
<p>Visit <a href='http://www.voyatrax.org/' rel='external'>The VoyaTrax CMS Project</a> site.</p>
<p><strong>ATTENTION :<b><i>The VoyaTrax Project is not affiliated in any way with the XOOPS project.</i></b><br><br></strong> Your site currently contains extended functionality, but if you want to add content: text pages, photo gallery, forum, links directory, ... You can download from the <a href='http://www.xoops.org/modules/repository' rel='external' title='Choice and download modules'>library of your local support</a> and install these components, it is the same for <a href='http://www.xoops.org/modules/extgallery' rel='external' title='Select and Install new themes'>design (theme) Additional</a>.</p>
";

$content .= "<h3>Security configuration</h3>
<p>The installer will try to configure your site for security considerations. Please double check to make sure:
<div class='confirmMsg'>
The <em>mainfile.php</em> is readonly.<br />
Remove the folder <em>{$installer_modified}</em> (or <em>install</em> if it was not renamed automatically by the installer)  from your server.
</div>
</p>
";
