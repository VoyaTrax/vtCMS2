<?php
// 
// _LANGCODE: en
// _CHARSET : UTF-8
// Translator: XOOPS Translation Team

$supports = array();

// Support site for English
$supports["english"] = array(
    "url"   => "http://www.voyatrax.org/",
    "title" => "English Support",
);

// Add extra support sites, use corresponding language folder name as key, for instance:

$supports["german"] = array(
    "url"   => "http://www.voyatrax.org/",
    "title" => "Deutsche Support"
);

