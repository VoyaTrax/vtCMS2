<?php
// 
// _LANGCODE: en
// _CHARSET : UTF-8
// Translator: XOOPS Translation Team
// License
define('LICENSE_NOT_WRITEABLE', 'License is %s . <br/><span style="color:#ff0000;">Make ../include/license.php Writable</span>');
define('LICENSE_IS_WRITEABLE', 'License is %s');
define('_INSTALL_WEBMASTER', 'Webmasters');
define('_INSTALL_WEBMASTERD', 'Webmasters of this site');
define('_INSTALL_REGUSERS', 'Registered Users');
define('_INSTALL_REGUSERSD', 'Registered Users Group');
define('_INSTALL_ANONUSERS', 'Anonymous Users');
define('_INSTALL_ANONUSERSD', 'Anonymous Users Group');
/**
 * New Group types
 */
define('_INSTALL_BANNEDUSERS', 'Banned Users');
define('_INSTALL_BANNEDUSERSD', 'Banned user group');
define('_INSTALL_MODERATORUSERS', 'Moderators');
define('_INSTALL_MODERATORUSERSD', 'These are Moderators for your website');
define('_INSTALL_SUBMITTERUSERS', 'Submitters');
define('_INSTALL_SUBMITTERUSERSD', 'This group can submit articles to your website');
define('_INSTALL_DEVELOPEUSERS', 'Developer');
define('_INSTALL_DEVELOPEUSERSD', 'This user has developer privileges and can see developer debugging messages.');
define('_INSTALL_L165', 'The site is currently closed for maintenance. Please come back later.');
define('_INSTALL_ANON', 'Anonymous');
define('_INSTALL_DISCLMR', 'While the administrators and moderators of this site will attempt to remove
or edit any generally objectionable material as quickly as possible, it is
impossible to review every message. Therefore you acknowledge that all posts
made to this site express the views and opinions of the author and not the
administrators, moderators or webmaster (except for posts by these people)
and hence will not be held liable.

You agree not to post any abusive, obscene, vulgar, slanderous, hateful,
threatening, sexually-orientated or any other material that may violate any
applicable laws. Doing so may lead to you being immediately and permanently
banned (and your service provider being informed). The IP address of all
posts is recorded to aid in enforcing these conditions. Creating multiple
accounts for a single user is not allowed. You agree that the webmaster,
administrator and moderators of this site have the right to remove, edit,
move or close any topic at any time should they see fit. As a user you agree
to any information you have entered above being stored in a database. While
this information will not be disclosed to any third party without your
consent the webmaster, administrator and moderators cannot be held
responsible for any hacking attempt that may lead to the data being
compromised.

This site system uses cookies to store information on your local computer.
These cookies do not contain any of the information you have entered above,
they serve only to improve your viewing pleasure. The email address is used
only for confirming your registration details and password (and for sending
new passwords should you forget your current one).

By clicking Register below you agree to be bound by these conditions.');

// TERMS,PRIVACY & IMPRINT - hyperclock
#define("_INSTALL_IMPRINT","Imprint");
#define("_INSTALL_PRIVACY","Privacy");
#define("_INSTALL_TERMS","Terms");

// About us - hyperclock
#define("_INSTALL_ABOUT","About Us");

// DISCLAIMER - hyperclock
#define("_INSTALL_DISCLAIMER","Disclaimer");

################
// IMPRINT - hyperclock
define('_INSTALL_IMPRINT','[b]According to § 5 TMG:[/b]
Max Example
Example Company
Example str. 0
00000 Example City

[b]Contact:[/b]
Telephone: +4900001234567
Fax: +4900001234567
Email: info@example.com

[b]Responsible for the content according to § 55 Abs. 2 RStV:[/b]
Max Example
Example str. 0
00000 Example City
Country

[b] Sources used for the images and graphics: [/b]
http: // N/A - Photograf: N/A

Source: [url=http: //www.e-recht24.de]http://www.e-recht24.de[/url]
[i]Translated using: [url=https://translate.google.com]Google Translator [/url][/i]');

// TERMS - hyperclock
define('_INSTALL_TERMS','[b]§1 Scope[/b]
The use of this website apply to the relationship between the user and the operator of the site (hereinafter the provider), the following terms and conditions. The use of the forum and the community functions is allowed only if the user accepts these terms and conditions.

[b]§2 Registration, participation, membership in the community[/b]
(1) is a prerequisite for the use of the forum and the community prior registration. With the successful registration, the user is a member of the community.
(2) There is no entitlement to membership.
(3) The user may access its third party to use not. The user is obliged to keep his access data secret and to protect against access by third parties.

[b]§3 Provider\'s services[/b]
(1) The provider allows the user to publish posts on his website under these Terms of Use. The provider provides the users contribution during his technical and economic opportunities free of charge with a discussion forum community features. The provider is trying to keep his service available. The provider accepts No further performance obligations. In particular, no claim of the user is on the constant availability of the service.
(2) The provider does not guarantee the accuracy, completeness, reliability, timeliness and usefulness of the content provided.

[b]§4 Disclaimer[/b]
(1) Claims for damages by the user are excluded, unless subsequently otherwise specified. The above disclaimer applies to the legal representatives and agents of the provider, if the user makes claims against these claims.
(2) Exempt from the laid down in paragraph 1 Disclaimer Claims for damages due to injury to life, limb, health, and claims for damages from the breach of contract. Material contractual obligations are those whose performance to achieve the objective of the Treaty is necessary. From the disclaimer is also excluded liability for damages based on an intentional or grossly negligent breach of duty of the provider, his legal representatives or agents.

[b]§5 Obligations of the user[/b]
(1) The user agrees to the provider not to publish posts that violate common decency or applicable law. The user undertakes in particular to not post,
[ul]
  [li]whose publication constitutes a criminal offense or is a misdemeanor,[/li]
  [li]infringe the copyright, trademark or competition law,[/li]
  [li]violate the Legal Services Act,[/li]
  [li]have offensive, racist, discriminatory or pornographic content,[/li]
  [li]containing advertising.[/li]
[/ul]
(2) In a breach of the obligation under paragraph 1, the supplier is entitled to amend the relevant posts or delete and block the user\'s access. The user is obliged to the seller to pay for damages caused by the breach of duty.
(3) The seller has the right to delete posts and content if it might contain a violation.
(4) The offeror to the user a right to exemption from third party claims were asserted for infringement of law by the user. The user undertakes to assist the provider in the defense of such claims. The user is also obliged to bear the costs of an adequate legal defense of the provider.

[b]§6 Transfer of rights of use[/b]
(1) The copyright remains with the posts set for each user. However, the user grants the seller with entering his contribution to the Forum the right to permanently ready hold the post on its website to retrieve and make them publicly available. The seller has the right to move posts within its website and to connect with other content.
(2) The user has no claim against the seller for cancellation or rectification created by him posts.
Google Übersetzer für Unternehmen:Translator ToolkitWebsite-Übersetzergoogle

[b]§7 Termination of Membership[/b]
(1) The user may terminate its membership by an appropriate declaration to the supplier without notice. Upon request, the supplier will then block the access of the user.
(2) The provider is entitled to terminate the membership of a user with a notice period of 2 weeks to the end of the month.
(3) For good cause exists, the provider is entitled to block the user\'s access immediately and to cancel the membership without notice.
(4) The provider is entitled upon completion of Mitglieschaft to terminate the user\'s access. The provider is entitled but not obliged, in the event of termination of membership to delete content created by users. A claim of the user to transfer the content created is excluded.

[b]§8 Modification or Adjustment of the offer[/b]
(1) The provider is entitled to make changes to its service.
(2) The provider is entitled to terminate his service by serving a notice period of 2 weeks. In the event of termination of his service, the provider is entitled but not obliged to delete content created by users.

[b]§9 Choice of law[/b]
On the contractual relationship between the provider and the user the right of the Federal Republic of Germany shall apply. Excluded from this choice of law are the mandatory consumer protection provisions of the country in which the user has his habitual residence.

[i]Source: [url=http://www.kluge-recht.de/muster-nutzungsbedingungen-forum.php]Terms of Use Internet Forum[/url][/i]

[i]Translated using: [url=https://translate.google.com]Google Translator [/url][/i]');

// PRIVACY - hyperclock
define('_INSTALL_PRIVACY','[b]Notice[/b]
The operators of these sites committed to protecting your personal data very seriously. We treat your personal data confidentially and in accordance with the legal data protection regulations as well as this privacy policy.

The use of our website is usually possible without providing personal information. As far as on our sides personal data (eg name, address or email addresses) are collected, this is as far as possible on a voluntary basis. These data are not without your express consent to third parties.
We point out that data transmission over the Internet (eg when communicating via email) can have security gaps. A complete protection of data against unauthorized access by third parties is impossible.


[b]Privacy Policy for the use of etracker[/b]
Our website uses the analytical service etracker. Provider is the etracker GmbH, Erste Brunnenstrasse 1 20459 Hamburg Germany.

From the data, user profiles can be created under a pseudonym. These cookies may be used. Cookies are small text files that are stored locally in the cache of your internet browser. The cookies make it possible to recognize your browser again. The data collected with the etracker technologies are not used without the explicit consent of the person concerned to identify visitors to our website and will not be merged with personal data about the bearer of the pseudonym.

The data collection and storage can be canceled at any time with effect for the future. To counter a data collection and storage of your visitor data for the future, you can refer the following link for an opt-out cookie of etracker, this causes are that the future is collected and stored no visitors data your browser etracker: [url=http://www.etracker.de/privacy?et=V23Jbb]http://www.etracker.de/privacy?et=V23Jbb[/url]

This opt-out cookie with the name "cntcookie" etracker is set. Please l oouml; exploiting that cookie does not, as long as you want to get your objection upright. For more information, refer to the privacy policy of etracker: [url=http: //www.etracker.com/de/datenschutz.html]http://www.etracker.com/de/datenschutz.html[/url]


[b]Privacy Policy for the use of Facebook plugins (like button)[/b]
On our pages are Plugins social network Facebook, vendors Facebook Inc., 1 Hacker Way, Menlo Park, California 94025, USA, integrated. The Facebook plugin, you agree to the Facebook logo or the "Like Button" ("Like") on our side. You can find an overview of the Facebook plugins here: [url=http://developers.facebook.com/docs/plugins/]http://developers.facebook.com/docs/plugins/[/url].
When you visit our pages, the plugin establishes a direct connection between your browser and the Facebook server is established. Facebook receives the information that you have with your IP address visited our site. If you click on the Facebook "Like" button while you are logged into your Facebook account, you can link the content of our pages on your Facebook profile. This allows Facebook to assign your visit to our website to your user account. We point out that we as providers of the sites no knowledge of the content of the transmitted data and use them through Facebook. For more information, please see the privacy policy of Facebook at [url=http://de-de.facebook.com/policy.php] http://de-de.facebook.com/policy.php[/url].

If you do not wish that Facebook visiting our pages to your Facebook users can assign account, please log out from your Facebook account.


[b]Privacy Policy for use of Google Analytics[/b]
This website uses functions of the web analytics service Google Analytics. Provider is the Google Inc. 1600 Amphitheatre Parkway Mountain View, CA 94043. Google Analytics uses. "Cookies". These are text files that are stored on your computer and enable analysis of your use of the website. The information generated by the cookie about your use of this website are usually transferred to a server of Google in the US and stored there.
In case of activation of IP anonymization on this website your IP address from Google will be truncated within Member States of the European Union or other parties to the Agreement on the European Economic Area. Only in exceptional cases is the full IP address is transmitted to a Google server in the US and shortened there. On behalf of the operator of this site Google will use this information to evaluate your use of the website, compiling reports on website activity and providing other with website and internet related services to the website operator. Is undertaken as part of Google Analytics that your browser sends IP address can not be merged with other data of Google.
You can refuse the use of cookies by selecting the appropriate settings on your browser software; however, we point out that you can optionally use to their full extent in this case not all features of this website. You can also prevent the data generated by the cookie and relating to your usage of the website (incl. Your IP address) to Google and the processing of these data by Google, by downloading available at the following link browser plugin and install: [url=http://tools.google.com/dlpage/gaoptout?hl=en]http://tools.google.com/dlpage/gaoptout?hl=de[/url]


[b]Privacy Policy for use of Google +1[/b]
Our pages use features of Google +1. Provider is the Google Inc. 1600 Amphitheatre Parkway Mountain View, CA 94043.

Collection and dissemination of information: The Google +1 button you can publish information worldwide. about the Google +1 button you and other users receive personalized content from Google and our partners. Google will store both the information that you have given for a content +1, as well as information about the page you viewed when you click +1. Your +1 can be shown as evidence along with your Profile name and photo in Google services, such as in search results or in your Google profile, or elsewhere on websites and ads on the Internet. Google records information about your +1 activities to improve Google\'s services for you and others. To use the Google +1 button, you need a globally visible, public Google profile that must contain at least the chosen name for the profile. This name is used in all Google services. In some cases, this name can also substitute any other name that you used when sharing content via your Google account. The identity of your Google profile can be shown users who know your e-mail address or other identifying information from you.
Using the information collected: Addition to the above uses the information you provide in accordance with the applicable Google privacy policy will be used. Google may release summary statistics on the +1 activities of users and sends it to users and partners, such as publishers, advertisers or linked sites.


[b]Privacy Policy for use of Instagram[/b]
Functions of the service Instagram are integrated on our sites. These functions are offered by Instagram Inc., 1601 Willow Road, Menlo Park, CA, 94025, US integrated. If you in your Instagram - are logged in account you can by clicking on the Instagram - buttons with your Instagram the content of our website - link profile. This allows Instagram assign to visit our pages to your user account. We point out that we as providers of the sites no knowledge of the content of the transmitted data and use them through Instagram.
For more information, please see the privacy statement of Instagram: [url=http://instagram.com/about/legal/privacy/]http://instagram.com/about/legal/privacy/[/url]


[b]Privacy Policy for use of LinkedIn[/b]
Our site uses functions of the network LinkedIn. Provider is the LinkedIn Corporation 2029 Stierlin Court, Mountain View, CA 94043. Every time a our pages that contain features of LinkedIn, connect to servers from LinkedIn is built. LinkedIn is informed that you have visited our website with your IP address. If you click the "Recommend" button LinkedIn and are logged into your account with LinkedIn, LinkedIn, it is possible to associate your visit to our website you and your account. We point out that we as providers of the sites have no knowledge of the content of the transmitted data and use them through LinkedIn.

For more information, please see the privacy policy of LinkedIn: [url=https://www.linkedin.com/legal/privacy-policy]https://www.linkedin.com/legal/privacy-policy[/url]


[b]Privacy Policy for use of Pinterest[/b]
On our side, we use social plugins from the social network Pinterest, which is from the Pinterest Inc., 635 High Street, Palo Alto, CA, 94301, USA ("Share") operated. When you visit a page that contains a social plugin, your browser makes a direct connection to the servers of Pinterest. The plugin transmits this log information to the server of options in the United States. This log data may include: your IP address, the address of the visited sites, which also contain Pinterest functions, type and settings of the browser, date and time of the request, for their use of options as well as cookies.

For more information about the purpose, scope and further processing and use of data by options as well as your rights and ways to protect your privacy can be found in the privacy notices of options: [url=https://about.pinterest.com/de/privacy-policy]https://about.pinterest.com/de/privacy-policy[/url]


[b]Privacy Policy for use of Tumblr[/b]
Our pages use buttons of the service Tumblr. Provider is the Tumblr, Inc., 35 East 21st St, 10th Floor, New York, NY 10010, USA. These buttons allow you to a post or page to share on Tumblr, or the seller to follow on Tumblr. If you call one of our websites with Share button, your browser establishes a direct connection to the servers of Tumblr. We have no influence on the amount of data that Tumblr with gathered by the plugin and transmitted. As things stand, the IP address of the user as well as the URL of the respective website are received.

More information can be found in the Privacy Statement of Share under [url=http://www.tumblr.com/policy/de/privacy]http://www.tumblr.com/policy/de/privacy[/url]


[b]Privacy Statement for the Use of Twitter[/b]
Functions of the Twitter service are integrated on our sites. These functions are offered by Twitter Inc., 1355 Market Street, Suite 900, San Francisco, CA 94103, USA. Through the use of Twitter and the function "Re-Tweet" the web pages you visit are linked to your Twitter account and made known to other users. This data is also transmitted to Twitter. We point out that we as providers of the sites no knowledge of the content of the transmitted data and use them through Twitter. For more information, please see the privacy statement of Twitter at [url=http: //twitter.com/privacy]http://twitter.com/privacy[/url].

Your privacy settings on Twitter, you can in the account settings under [url=http://twitter.com/account/settings]Change http://twitter.com/account/settings[/url].


[b]Privacy Policy for use of Xing[/b]
Our website uses functions of the network XING. Provider is the XING AG, Dammtorstraße 29-32, 20354 Hamburg, Germany. Every time a our pages that contain features of Xing, connect to servers from Xing is produced. Storage of personal data is not carried out according to our knowledge. In particular, no IP addresses are stored or evaluated the usage behavior.

Further information on data protection and the Xing Share button, refer to the privacy policy of Xing under [url=https://www.xing.com/app/share?op=data_protection]https://www.xing.com/app/share?op=data_protection[/url]

[b]Information, erasure or blocking[/b]
You always have the right to free information about your stored personal data, its origin and destination and the purpose of data processing as well as a right to correction, blocking or deletion of this data. For this purpose, as well as other questions relating to personal data, you can contact at any time at the address given in the imprint on us.


[b]Server log files[/b]
The provider of the pages automatically collects and stores information in so-called server log files, that your browser transmits to us automatically. These are:
[ul]
    [li] Browser type / browser version [/li]
    [li] Operating system [/li]
    [li] Referrer URL [/li]
    [li] Host name of accessing computer [/li]
    [li] Time of server request [/li]
[/ul]
These data are not specific individuals. A reunion of these data with other data sources is not made. We reserve the right to examine these data retrospectively if us concrete evidence of unlawful use are known.


[b]Cookies[/b]
The websites use some so-called cookies. Set cookies on your computer no harm and do not contain viruses. Cookies are used to make our offer more user-friendly, effective and safer. Cookies are small text files that are stored on your computer and saved by your browser.

Most of the cookies we use are so-called "session cookies". They are automatically deleted after your visit. Other cookies remain on your device memory until you delete them. These cookies allow us to recognize your browser on your next visit.

You can set your browser so that you are informed about the use of cookies and allow cookies only in individual cases, activate the acceptance of cookies in certain cases or general rule and the automatic deletion of cookies when you close the browser. Upon deactivation of cookies, the functionality of this website may be restricted.



[b]Contact[/b]
If you send us via the contact form requests, your data from the Request Form, including the contact details provided by you will be stored there for the purpose of processing the request and in the case of follow-up questions with us. These data will not be disclosed without your consent on.



[b]Contradiction promotional emails[/b]
The use of published under the imprint obligation for sending unsolicited advertisements and information materials is hereby rejected. The operators of these pages expressly legal steps in case of unsolicited promotional information, such as spam e-mails.



[b]Newsletter Data[/b]
If you want to receive the newsletter offered on the website, we need your e-mail address and information that allow us to verify that you are the owner of the e-mail address and agree to receive the newsletter are , Further data is not collected. We use this information exclusively for sending the requested information and pass it on to third parties.

The consent for storing the data, the e-mail address as well as their use for sending the newsletter can be revoked at any time, for example through the "unsubscribe" link in the newsletter.



[i]Source: [url=http://www.e-recht24.de/muster-datenschutzerklaerung.html]eRecht24[/url][/i]

[i]Translated using: [url=https://translate.google.com]Google Translator[/url][/i]');

// ABOUT - hyperclock
define('_INSTALL_ABOUT','[b][i]VoyaTraX Content Management System[/i] (vtCMS)[/b] is an open-source Object-Oriented Web publishing system written in PHP. 

It is an ideal tool for developing small to large dynamic community websites, intra company portals, corporate portals, weblogs and much more.

vtCMS is released under the terms of the [url=http://www.gnu.org/copyleft/gpl.html]GNU General Public License (GPL)[/url] and is free to use and modify.
It is free to redistribute as long as you abide by the distribution terms of the GPL.');

// DISCLAIMER - hyperclock
define('_INSTALL_DISCLAIMER','[b]Liability for Content[/b]
As a service provider we are responsible according to § 7 Abs.1 TMG for own contents on these pages under the general laws. According to §§ 8 to 10 TMG we are not obligated as a service provider to monitor transmitted or stored foreign information or to investigate circumstances that indicate illegal activity. Obligations to remove or block the use of information under the general laws remain unaffected. However, a relevant liability is only possible from the date of knowledge of a specific infringement. Upon notification of such violations, we will immediately remove this content.



[b]Liability for links[/b]
Our site contains links to external websites, over which we have no influence. Therefore we can not accept any liability for this external content. The respective provider or operator of such sites is always responsible for the contents of any Linked Site. The linked sites were checked at the time of linking for possible legal violations. Illegal contents were at the time of linking. A permanent control of the linked pages is unreasonable without concrete evidence of a violation. Upon notification of violations, we will remove such links immediately.



[b]Copyright[/b]
The contents and works on these pages created by the site operators are subject to German copyright law. The reproduction, adaptation, distribution and any kind of exploitation outside the limits of copyright require the written consent of the respective author or creator. Downloads and copies of these pages are only permitted for private, non-commercial use. Insofar as the content is not created by the website operator, the property rights of third parties. In particular contents of third parties are marked as such. Should you nevertheless become aware of copyright infringement, we ask for a hint. Upon notification of violations, we will remove such content immediately.



Source: [url=http: //www.e-recht24.de]http://www.e-recht24.de[/url]

[i]Translated using: [url=https://translate.google.com]Google Translator[/url][/i]');
